import { Modal, Button, Form, ListGroup, Image } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({ products, fetchData }){

	const [productId, setProductId] = useState('')
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantityAvailable, setQuantityAvailable] = useState('');
	// const [existingImages, setExistingImages] = useState([]);
	const [image, setImages] = useState([]);

	const [showEdit, setShowEdit] = useState(false);

	// const handleImageChange = (e) => {

	//     const files = e.target.files;
	//     setImages([...images, ...files]);

	// };

	// const removeImage = (index) => {
	//     const newImages = [...images];
	//     newImages.splice(index, 1);
	//     setImages(newImages);
	// };

	// const removeExistingImage = (index) => {
	//     const newExistingImages = [...existingImages];
	//     newExistingImages.splice(index, 1);
	//     setExistingImages(newExistingImages);
	// };

	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
	    	headers: {
	    	  Authorization: `Bearer ${ localStorage.getItem('token') }`
	    	}
	    })
		.then(res => res.json())
		.then(data => {
			
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantityAvailable(data.quantityAvailable);
			// setExistingImages(data.images || []);
			setImages(data.images[0]);
		})

		// Open the modal
		setShowEdit(true)
	}

	const closeEdit = () =>{
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
		setQuantityAvailable(0);
		setImages('');
	}

	
	const editProduct = (e, productId) => {
		e.preventDefault();

		// Create a FormData object to handle file uploads
	    // const formData = new FormData();
	    // formData.append('name', name);
	    // formData.append('description', description);
	    // formData.append('price', price);
	    // formData.append('quantityAvailable', quantityAvailable);

	    // // Append new images
	    // for (let i = 0; i < images.length; i++) {
	    //     formData.append('images', images[i]);
	    // }

	    // // Append existing images
        // for (let i = 0; i < existingImages.length; i++) {
        //     formData.append('existingImages', existingImages[i]);
        // }

        // console.log(images);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name,
				description,
				price,
				quantityAvailable,
				image
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated'
				})
				closeEdit();
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				closeEdit();
				fetchData();
			}
		})
	}


	return(

		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(products)}> Edit </Button>

	
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
                   <Modal.Header closeButton>
                       <Modal.Title>Edit Product</Modal.Title>
                   </Modal.Header>
                   <Modal.Body>    
                       <Form.Group>
                           <Form.Label>Name</Form.Label>
                           <Form.Control 
                           		type="text"
                           		value={name}
                           		onChange={e => setName(e.target.value)} 
                           		required/>
                       </Form.Group>
                       <Form.Group>
                           <Form.Label>Description</Form.Label>
                           <Form.Control 
                           		type="text"
                           		value={description}
                           		onChange={e => setDescription(e.target.value)} 
                           		required/>
                       </Form.Group>
                       <Form.Group>
                           <Form.Label>Price</Form.Label>
                           <Form.Control 
                           		type="number"
                           		value={price}
                           		onChange={e => setPrice(e.target.value)} 
                           		required/>
                       </Form.Group>
                       <Form.Group>
                           <Form.Label>Quantity</Form.Label>
                           <Form.Control 
                           		type="number"
                           		value={quantityAvailable}
                           		onChange={e => setQuantityAvailable(e.target.value)} 
                           		required/>
                       </Form.Group>
                       <Form.Group>
                       	<Form.Label>Image:</Form.Label>
                       	<Form.Control 
                           		type="text"
                           		value={image}
                           		onChange={e => setDescription(e.target.value)} 
                           		required/>
                       	{/*<Form.Control
                       		type="file"
                       		accept="image/*"
                       		multiple
                       		onChange={handleImageChange}
                       		/>
                       	<ListGroup className="mt-2">
                       	    {(existingImages || []).map((image, index) => (
                       	        <ListGroup.Item key={index} className="d-flex justify-content-between align-items-center">
                       	            <div>
                       	                <Image src={image} alt={`Image ${index + 1}`} rounded width={50} height={50} className="mr-2" />
                       	                {image}
                       	            </div>
                       	            <div>
                       	            	<Button variant="danger" size="sm" onClick={() => removeExistingImage(index)}>
                       	                                Remove
                       	                            </Button>
                       	            </div>
                       	        </ListGroup.Item>
                       	    ))}
                       	    {images.map((image, index) => (
                       	        <ListGroup.Item key={index} className="d-flex justify-content-between align-items-center">
                       	            <div>
                       	                <Image src={URL.createObjectURL(image)} alt={`Image ${index + 1}`} rounded width={50} height={50} className="mr-2" />
                       	                {image.name}
                       	            </div>
                       	            <div>
                       	                <Button variant="danger" size="sm" onClick={() => removeImage(index)}>
                       	                    Remove
                       	                </Button>
                       	            </div>
                       	        </ListGroup.Item>
                       	    ))}
                       	</ListGroup>*/}
                       </Form.Group>
                   </Modal.Body>
                   <Modal.Footer>
                       <Button variant="success" type="submit">Update</Button>
                   </Modal.Footer>
               </Form>
           </Modal>
		</>

	)

}