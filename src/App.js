
import './App.css';
import { ParallaxProvider } from 'react-scroll-parallax';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavBar from './components/AppNavBar/AppNavBar';

// Pages
import Home from './pages/Home/Home';
import Register from './pages/RegisterLogin/Register';
import Login from './pages/RegisterLogin/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import OrderHistory from './pages/OrderHistory';
import Cart from './pages/Cart';
import Users from './pages/Users';
import Profile from './pages/Profile';



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {

      console.log(data);

      if(typeof data._id !== "undefined"){
        
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

  }, [])

  return (
      <UserProvider value={{ user, setUser, unsetUser }}>
      <ParallaxProvider>
      <Router>
        <AppNavBar />
        <Container id='body-container'>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/products" element={<Products />} />
            <Route path="/history" element={<OrderHistory />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/users" element={<Users />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout/>} />
          </Routes>
        </Container>
      </Router>
      </ParallaxProvider>
      </UserProvider>
  );
}

export default App;
