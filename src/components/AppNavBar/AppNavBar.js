// React Bootstrap Component
import { NavLink } from 'react-router-dom';
import { useContext, useEffect, useState, useRef } from 'react'
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../../UserContext';
import './AppNavBar.scss';

import logo from './assets/thriftharbor.png';

export default function AppNavBar() {


	const { user } = useContext(UserContext);
	const [lastScrollY, setLastScrollY] = useState(window.scrollY);

	useEffect(() => {
	
		const nav = document.querySelector(".nav");
		
		const handleScroll = () => {

			if (lastScrollY < window.scrollY) {
				nav.classList.add("nav--hidden");
			} else {
				nav.classList.remove("nav--hidden");
			}
			setLastScrollY(window.scrollY);
		};
	
		window.addEventListener("scroll", handleScroll);

		return () => {
		  window.removeEventListener("scroll", handleScroll);
		};

	 }, [lastScrollY]);

	return (

		<Navbar id="navbar" expand="lg" className="nav">
	        <Navbar.Brand href="/" id="brand"><img id='logo-png' src={logo}/></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
	        <Navbar.Collapse id="basic-navbar-nav" className="py-5 nav-collapse text-center justify-content-end">
	            <Nav>
	            	<Nav.Link as={NavLink} to="/products" id="linkNav"  exact>Products</Nav.Link>
		           	{(user.id === null) ?
		           		<>
		           		<Nav.Link as={NavLink} to="/register" id="linkNav" exact>Register</Nav.Link>
		           		<Nav.Link as={NavLink} to="/login" id="linkNav" exact>Log In</Nav.Link>
		            	</>
		           	:
		           		<>
		           		{user.isAdmin ?
			            	<>
			            		<Nav.Link as={NavLink} to="/history" id="linkNav" exact>History</Nav.Link>
				            	<Nav.Link as={NavLink} to="/users" id="linkNav" exact>Users</Nav.Link>
				            </>
				            :
				            <>
				            	<Nav.Link as={NavLink} to="/cart" id="linkNav" exact>Cart</Nav.Link>
				            	<Nav.Link as={NavLink} to="/profile" id="linkNav" exact>Profile</Nav.Link>
				            </>
		            	}
		            	<Nav.Link as={NavLink} to="/logout" id="linkNav" exact className="mx-3">Logout</Nav.Link>
		           		</>
		           	}
	            </Nav>
	        </Navbar.Collapse>
		</Navbar>

	)
} 