import { Container, Row, Col, Image, Tabs, Tab } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import OrderHistory	from './OrderHistory';
import UpdateProfile from '../components/UpdateProfile';
import EditPassword from '../components/EditPassword';
import { Link } from 'react-router-dom';


export default function Profile(){


	const [userId, setUserId] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [images, setImages] = useState([]);
	const [history, setHistory] = useState([]);


	const fetchData = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers : {
				Authorization: `Bearer ${ localStorage.getItem( 'token' ) }`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUserId(data._id);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);
			setImages(data.images);
			setHistory(data.orderedProduct.products);

		})

	}

	useEffect(() => {

		fetchData();

	},[])


	return(

		<>
	      <Row className="my-4 d-flex align-items-center justify-content-center" >
	      	<Col md={4} className="text-center">
	          <Image src={`${ process.env.REACT_APP_API_URL }/images/${images[0]}`} alt="Profile" roundedCircle fluid style={{width: '180px', height: '180px', objectFit: 'cover'}} />
	        </Col>
	        <Col md={4} className="text-center mt-2">
	        	<div>
		          <h3>{`${firstName} ${lastName}`}</h3>
		          <p>Email: {email}</p>
		          <p>Mobile Number: {mobileNo}</p>
		          <EditPassword user={userId}/>
		         </div>
	        </Col>
	      </Row>
	      <Tabs defaultActiveKey="history" id="user-profile-tabs" className="mt-3" justify>
	      	<Tab eventKey="history" title="Order History">
	      	{history.length > 0 ? (
	      	  	<OrderHistory/>
	      	) : (

	      		<Container fluid className="d-flex flex-column align-items-center justify-content-center">
	      			<h1 className="text-muted text-center mt-5">Order History is Empty</h1>
	      			<Link className="btn btn-primary mt-3" to={`/products`}>Purchase Now!</Link>
	      		</Container>

	      	)
	      	}
	      	</Tab>
	        <Tab eventKey="update" title="Edit Profile">
	          <UpdateProfile fetchData={fetchData} />
	        </Tab>
	      </Tabs>
		</>

	)

}