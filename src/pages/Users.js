import { useState, useEffect } from 'react';
import { Table, Container, Row } from 'react-bootstrap';
import SetAdmin from '../components/SetAdmin';
import '../App.css';

export default function Users(){

	const [users, setUsers] = useState([]);
	const [userList, setUserList] = useState([]);


	const fetchData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
		    
		    console.log(data);

		    setUsers(data);


		});

	}


	useEffect(() => {

		fetchData();

	},[])

	useEffect(() => {

		const usersArr = users.map(users => {

			return (
			    <tr key={users._id}>
			        <td data-label="ID">{users._id}</td>
			        <td data-label="First Name">{users.firstName}</td>
			        <td data-label="Last Name">{users.lastName}</td>
			        <td data-label="Email">{users.email}</td>
			        <td data-label="User Category">
			            {users.isAdmin ? "Admin" : "User"}
			        </td>
			        <td data-label="Mobile Number">{users.mobileNo}</td>
			        <td data-label="Shipping Address">{users.shippingAddress}</td>
			        <td data-label="Actions"><SetAdmin usersData={{ userId: users._id, isAdmin: users.isAdmin }} fetchData={fetchData}/></td>
			    </tr>
			)

		})

		setUserList(usersArr);

	},[users])

	return(

		<>
		<Row id="productsBack">
		    <h1 className="text-center my-4 text-light"> Admin Dashboard</h1>
		    <Container className="d-flex align-items-center justify-content-center">
		    </Container>
		    <Table striped bordered hover responsive className="mt-4">
		        <thead>
		            <tr className="text-center">
		                <th>ID</th>
		                <th>First Name</th>
		                <th>Last Name</th>
		                <th>Email</th>
		                <th>User Category</th>
		                <th>Mobile Number</th>
		                <th>Shipping Address</th>
		                <th colSpan="2">Actions</th>
		            </tr>
		        </thead>

		        <tbody>
		            {userList}
		        </tbody>
		    </Table> 
		</Row>   
		</>

	)

}