import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Login() {

	
	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {

		// Prevent page redirection via form submission
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			}) 
		})
		.then(res => res.json())
		.then(data => {
			if(data.access){

				// Set the token of the authenticated user in the local storage
				localStorage.setItem("token", data.access);

				// function for retrieving user details
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to the Thrift Shop"
				});

				navigate("/products");

			} else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again"
				});
			}
		})

		// Clearing our input fields after submission
		setEmail('');
		setPassword('');

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})

	}

	useEffect(() => {

		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password])

	return (
		(user.id !== null) ?
		<Navigate to='/products' />
		:
		<Container id='form-container' className='col-md-6'>
			<Form onSubmit={(e) => authenticate(e)} id='form-form'>
			<h2 className="my-5 text-center" id='title'>Login</h2>
				<Form.Group controlId="userEmail" className='form-group'>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
						className='form-control'
					/>
				</Form.Group>

				<Form.Group controlId="password" className='form-group'>
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password" 
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						required
						className='form-control'
					/>
				</Form.Group>


				{ isActive ?
					<Button type="submit" id="submitBtn" className="my-2 form-button">
					Submit
					</Button>
				:

					<Button type="submit" id="submitBtn" className="my-2 form-button" disabled>
					Submit
					</Button>
				}
			</Form>
		</Container>
	)
}