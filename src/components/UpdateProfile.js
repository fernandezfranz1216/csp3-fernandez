import { Form, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';

export default function UpdateProfile({fetchData}) {

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ mobileNo, setMobileNo ] = useState("");
	const [ images, setImages ] = useState([]);


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers : {
				Authorization: `Bearer ${ localStorage.getItem( 'token' ) }`
			}
		})
		.then(res => res.json())
		.then(data => {

			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);
			setImages(data.images);
		})
	},[])

	console.log(images);

	const handleImageChange = (e) => {

	    const files = e.target.files;
	    setImages(files);

	};

	const editUser = (e) => {

		e.preventDefault();

		const formData = new FormData();
		formData.append('firstName', firstName);
		formData.append('lastName', lastName);
		formData.append('email', email);
		formData.append('mobileNo', mobileNo);

		for (let i = 0; i < images.length; i++) {
		    formData.append('images', images[i]);
		}

		console.log(images[0]);

		fetch(`${process.env.REACT_APP_API_URL}/users/edit`, {
			method: "PUT",
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: formData
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Profile Updated'
				})
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				fetchData();
			}

		})

	}

	return (

		<Form onSubmit={(e) => editUser(e)} className="mt-4">
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter First Name" 
					required
					value={firstName}
					onChange={e => {setFirstName(e.target.value)}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter Last Name" 
					required
					value={lastName}
					onChange={e => {setLastName(e.target.value)}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter Email" 
					required
					value={email}
					onChange={e => {setEmail(e.target.value)}}
					/>					
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Enter 11 Digit No."
					required
					value={mobileNo}
					onChange={e => {setMobileNo(e.target.value)}}
					/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Profile Picture:</Form.Label>
				<Form.Control type="file" accept="image/*" onChange={handleImageChange}	/>
			</Form.Group>
			<Button variant="primary" type="submit" id="submitBtn" className="my-2">Submit</Button>
		</Form>

	)

}