import React, { useState, useEffect, useContext } from 'react';
import HistoryCard from '../components/HistoryCard';
import HistoryTable from '../components/HistoryTable';
import {Row} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function OrderHistory() {

	const [history, setHistory] = useState([]);

	const { user } = useContext(UserContext);

	useEffect(() => {

	    fetch(`${process.env.REACT_APP_API_URL}/users/${user.isAdmin ? "all-orders" : "orders"}`, {
	    	headers: {
	    	  Authorization: `Bearer ${ localStorage.getItem('token') }`
	    	}
	    })
	    .then(res => res.json())
	    .then(data => {
	        
	        console.log(data);
	        setHistory(data);

	    });

	},[])

	console.log(history);


	return (
		<>
			<Row id="productsBack">
				{ user.isAdmin ? (
					<HistoryTable historyData={history}/>
					) : (
					<HistoryCard historyData={history}/>
					)
				}
			</Row>
		</>

	)

}