import { useState, useEffect, useContext } from 'react';
import { Row } from 'react-bootstrap';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';


export default function Products() {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [isLoading, setIsLoading] = useState(false);

	const fetchData = () => {
		return new Promise((resolve, reject) => {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
				headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
				}
			})
			.then(res => res.json())
			.then(data => {
				
				setProducts(data);
				resolve();
			})
			.catch(error => {
				console.error(error);
				reject(error);
			})
		});
	}

	useEffect(() => {
		setIsLoading(true);
	    fetchData().then(() => {
			setIsLoading(false);
		});
 
	}, []);


	return(

		<Row id="productsBack">
		<>
			{
				user.isAdmin ?
					<AdminView productsData={products} fetchData={fetchData}/>
					:
					<UserView productsData={products} isLoading={isLoading} fetchData={fetchData}/>
			}
		</>
		</Row>

	)

}