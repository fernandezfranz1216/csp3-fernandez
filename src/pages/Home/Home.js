import { useEffect, useState} from 'react';
import {Row, CardGroup} from 'react-bootstrap';
import Banner from '../../components/Banner/Banner';
import ProductCard from '../../components/ProductCard/ProductCard';
import './Home.scss';

export default function Home() {

    const data = {
        title: '"Fashion finds for the thrifty minds..."',
        destination: "/products"
    }
    const [previews, setPreviews] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
    const featuredID = 'featured';

	const fetchData = () => {
		return new Promise((resolve, reject)=>{
			fetch(`${ process.env.REACT_APP_API_URL}/products/all`,{
				headers: {
					Authorization: `Bearer ${ localStorage.getItem(`token`) }`
				}
			})
			.then(res => res.json())
			.then(data => {
				const filteredData = data.filter(item => item.isActive);

				const featured = [];
	
				const numbers = [];
	
				const generateRandomNumbers = () => {
	
					let randomNum = Math.floor(Math.random() * filteredData.length);
	
					if(numbers.indexOf(randomNum) === -1) {
						numbers.push(randomNum);
					} else {
						generateRandomNumbers();
					}
				}
				console.log(numbers);
	
				for (let i = 0; i < 3; i++) {
	
					generateRandomNumbers();
	
					featured.push(
	
						<ProductCard productProp={filteredData[numbers[i]]} isLoading={isLoading} key={filteredData[numbers[i]]._id} fetchData={fetchData} className='px-2'/>
	
						)
				}
				setPreviews(featured);
				resolve();
			})
			.catch(error => {
				console.error(error);
				reject(error);
			})
		})
	}

	useEffect(() => {
		setIsLoading(true);
	    fetchData().then(() => {
			setIsLoading(false);
		});
	}, [])

    return (
        <>
			<Row id='hero' className='d-flex align-items-center justify-content-center'>
            	<Banner data={data} featuredID={featuredID}/>
			</Row>
			<Row id={featuredID}>
				<h1 className='text-center py-4'>Featured Items</h1>
				<CardGroup className="justify-content-between py-5">
					{previews}
				</CardGroup>
			</Row>
        </>
    )
}