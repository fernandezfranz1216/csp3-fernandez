import { useState, useEffect } from 'react';
import CartProduct from './CartProduct';
import { Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CartList({ productsData, fetchData, checkoutAll }) {

	const [list, setList] = useState([]);

	useEffect(() => {

		if (productsData && productsData.basket) {

			const productsArr = productsData.basket.map(product => {

				console.log(product);

				return(
					<CartProduct productProp={product} key={product.productId}/>
				)

			})

			setList(productsArr);
		}

	},[productsData])

	return (
		<>
			{list.length > 0 ? (
				<>
					{list}
					<Button variant="success" onClick={() => checkoutAll()}>Checkout All</Button>
				</>
			) : (
			<>
				<Container className="d-flex flex-column align-items-center justify-content-center vh-100">
					<h1 className="text-muted text-center mt-5">Cart is Empty</h1>
					<Link className="btn btn-primary mt-3" to={`/products`}>Add to cart now!</Link>
				</Container>
			</>

			)}
		</>

	)

}