import { Row, Col, Container, Image, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './Banner.scss';

import sailor from './assets/sailor-man.png';

export default function Banner({data, featuredID}) {
    const {title} = data;

    return (
        <>
        <Col id="landing-img" md={6}>
            <Image src={sailor} fluid/> 
        </Col>
        <Col id="landing-text" md={6} className="text-md-end">
            <h1 id="title">{title}</h1>
            <Button id='to-featured' href={'#'+featuredID}>Check it out</Button>
        </Col>
        </>
    )
}