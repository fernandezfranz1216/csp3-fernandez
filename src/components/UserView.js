import { useState, useEffect } from 'react';
import { Card, Col, Row, Placeholder } from 'react-bootstrap';
import ProductCard from './ProductCard/ProductCard';


export default function UserView({productsData, isLoading, fetchData}) {

    console.log('userview isloading:' + isLoading);
    
    if(isLoading){
        return(
            <Row>
            {Array.from({length:9}).map((_,index)=>(
                <Col md={4} className='text-center' key={index}>
                    <Card className='my-2' id='productCard'>
                    <div className="placeholder-card" style={{ backgroundColor: 'lightgray' }}>
                        <Card.Img variant="top" src="https://via.placeholder.com/150x150" />
                        <Card.Body>
                            <Placeholder as={Card.Title} animation="glow">
                                <Placeholder  xs={6}/>
                            </Placeholder>
                            <Placeholder as={Card.Subtitle} animation="glow">
                                <Placeholder xs={7}/>
                            </Placeholder>
                        </Card.Body>
                    </div>
                    </Card>
                </Col>
            ))}
            </Row>
        );
    }

    return(
        <>
            {/*<CourseSearch />*/}
        <Row>
            {
                productsData.map(product => {


                    if(product.isActive === true) {
                        return (
                            <ProductCard productProp={product} isLoading={isLoading} key={product._id} fetchData={fetchData}/>
                        )
                    } else {
                        return null;
                    }
                })
            }
        </Row>
            {/*<SearchByPrice />*/}
        </>
        )
}