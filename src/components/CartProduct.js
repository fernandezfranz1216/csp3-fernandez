import { Card, Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CartProduct({productProp}) {

	console.log(productProp);

	const { productName, productPrice, quantity, productImages, subtotal } = productProp;


	return(

		<Card className="my-2">
			<Row className="g-0">
				<Col md={4}>
					<Card.Img src={`${ process.env.REACT_APP_API_URL }/images/${productImages[0]}`} fluid/>
				</Col>
				<Col md={8}>
					<Card.Body>
						<Card.Title>{productName}</Card.Title>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {productPrice}</Card.Text>
						<Card.Subtitle>Quantity:</Card.Subtitle>
						<Card.Text>{quantity}</Card.Text>
						<Card.Subtitle>Total Price:</Card.Subtitle>
						<Card.Text>Php {subtotal}</Card.Text>
					</Card.Body>
				</Col>
			</Row>
		</Card>

	)

}

CartProduct.propTypes = {
	
	productProp: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		subtotal: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
		productImages: PropTypes.arrayOf(PropTypes.string).isRequired,
		productPrice: PropTypes.number.isRequired
	})

}