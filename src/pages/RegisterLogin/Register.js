import { useState, useEffect, useContext } from 'react';
import { Form, Button, OverlayTrigger, Tooltip, Container } from 'react-bootstrap';
import UserContext from '../../UserContext'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import './RegisterLogin.scss';

export default function Register() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ mobileNo, setMobileNo ] = useState("");
	const [ password, setPassword] = useState("");
	const [images, setImages] = useState([]);
	const [ confirmPassword, setConfirmPassword ] = useState("");

	const [ isActive, setIsActive ] = useState(false);
	const [ message, setMessage ] = useState('');

	function registerUser(e) {

		e.preventDefault();

		// Create a FormData object to handle file uploads
		const formData = new FormData();
			formData.append('firstName', firstName);
			formData.append('lastName', lastName);
			formData.append('email', email);
			formData.append('mobileNo', mobileNo);
			formData.append('password', password);
			formData.append('images', images);

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			body: formData
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data) {
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setImages(null);
				setConfirmPassword("");

				Swal.fire({
					title: "Registration Successful",
					icon: "success"

				});

				navigate("/");


			} else {

				Swal.fire({
					title: "Registration Unsuccessful",
					icon: "error",
					text: data.message
				});
			}
		})
	}


	useEffect(() => {

		const passwordRegex = /^(?=.*[!@#$%^&*(),.?":{}|<>])(?=.*[a-zA-Z0-9]).{8,}$/;

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (mobileNo.length === 11)) {

			if (password === confirmPassword) {

				if (passwordRegex.test(password)){

					setIsActive(true);

				} else {

					setIsActive(false);
					setMessage('Password must be at least 8 characters long and contain at least one special character');

				}

			} else {

				setIsActive(false);
				setMessage('Passwords does not match');

			}

		} else {

			setIsActive(false)
			setMessage('Please complete personal details');

		}

		// dependencies
	}, [firstName, lastName, email, mobileNo, password, confirmPassword])

    return (
		(user.id !== null) ?
		<Navigate to='/products' />
		:
		<Container id='form-container' className='col-md-6'>
			<Form onSubmit={(e) => registerUser(e)} id='form-form'>
			<h2 className="mt-3 mb-3 text-center" id='title'>Register</h2>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>First Name:</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Enter First Name" 
						required
						value={firstName}
						onChange={e => {setFirstName(e.target.value)}}
						className='form-control'
					/>
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>Last Name:</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Enter Last Name" 
						required
						value={lastName}
						onChange={e => {setLastName(e.target.value)}}
						className='form-control'
					/>
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>Email:</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter Email" 
						required
						value={email}
						onChange={e => {setEmail(e.target.value)}}
						className='form-control'
						/>					
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>Mobile No:</Form.Label>
					<Form.Control 
						type="text"
						inputMode='numeric'
						placeholder="Enter 11-digit Phone number"
						onInput={(e) => {const input = e.target.value.replace(/[^0-9]/g,''); e.target.value = input;}}
						required
						value={mobileNo}
						onChange={e => {setMobileNo(e.target.value)}}
						className='form-control'
						/>
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label>Profile Picture:</Form.Label>
					<Form.Control type="file" accept="image/*" multiple onChange={(e) => setImages(e.target.files)} className='form-control' />
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>Password:</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Enter Password" 
						required
						value={password}
						onChange={e => {setPassword(e.target.value)}}
						className='form-control'
						/>
				</Form.Group>
				<Form.Group className='form-group'>
					<Form.Label><span className='asterisk'>*</span>Confirm Password:</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Confirm Password" 
						required
						value={confirmPassword}
						onChange={e => {setConfirmPassword(e.target.value)}}
						className='form-control'
						/>
				</Form.Group>

				{
					isActive
					?
					<Button type="submit" id="submitBtn" className="my-2 form-button">Submit</Button>
					:
					<>
						<OverlayTrigger overlay={<Tooltip id="tooltip-disabled">{message}</Tooltip>}>
							<span className="d-inline-block">
								<Button type="submit" id="submitBtn" className="my-2 form-button" disabled>Submit</Button>
							</span>
						</OverlayTrigger>
					</>
				}					
			</Form>
		</Container>
    )

}