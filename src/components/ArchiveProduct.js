import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ArchiveProduct({ products, fetchData }){

	const { isActive, productId } = products;

	function archiveToggle(productId){

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
			  Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated'
				})
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				fetchData();
			}

		})

	}


	function activateToggle(productId){

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
			method: "PUT",
			headers: {
			  Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Successfully Updated'
				})
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				fetchData();
			}

		})

	}


	return(

		isActive ?
		<Button variant="danger" size="sm" onClick={() => archiveToggle(productId)}>Archive</Button>
		:
		<Button variant="success" size="sm" onClick={() => activateToggle(productId)}>Activate</Button>

	)

}