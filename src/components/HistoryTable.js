import { Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import '../App.css';


export default function HistoryTable({historyData}) {

	const [history, setHistory] = useState([]);

	useEffect(() => {

		const sortedHistory = [...historyData].sort((a, b) => new Date(b.purchasedOn) - new Date(a.purchasedOn));

	    if (historyData && historyData.length > 0) {
	        const historyArr = sortedHistory.map(hist => (
	            <tr key={hist._id}>
	                <td data-label="ID">{hist.productId}</td>
	                <td data-label="Name">{hist.productName}</td>
	                <td data-label="Description">{hist.quantity}</td>
	                <td data-label="Price">{hist.productPrice}</td>
	                <td data-label="Status">{hist.status}</td>
	                <td data-label="Purchase Date">{hist.purchasedOn}</td>
	            </tr>
	        ));
	        setHistory(historyArr);
	    }
	    
	}, [historyData]);

	return(

		<>
		    <h1 className="text-center my-4 text-light"> Order History</h1>
		    <Table striped bordered hover responsive className="mt-4">
		        <thead>
		            <tr className="text-center">
		                <th>ID</th>
		                <th>Name</th>
		                <th>Quantity</th>
		                <th>Price</th>
		                <th>Status</th>
		                <th>Purchase Date</th>
		            </tr>
		        </thead>

		        <tbody>
		            {history}
		        </tbody>
		    </Table>    
		</>

	)

}