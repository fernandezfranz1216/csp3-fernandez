import { useState, useEffect } from 'react';
import CartList from '../components/CartList';
import Swal from 'sweetalert2';

export default function Cart(){

	const [products, setProducts] = useState({});

	const fetchData = () => {

		fetch(`${ process.env.REACT_APP_API_URL }/carts/cart`,{
			headers: {
			  Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			setProducts(data);

		})

	}

	useEffect(() => {

		fetchData();

	},[])

	console.log(products);


	function checkoutAll(){

		fetch(`${ process.env.REACT_APP_API_URL }/carts/checkout-all`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data.message === "Checkout Successful"){

				Swal.fire({
					title: "Checkout Successful",
					icon: "success",
					text: "You have successfully purchased this product."
				})

				fetchData();

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})

				fetchData();

			}

		})

	}


	return(
		<>
			<CartList productsData={products} fetchData={fetchData} checkoutAll={checkoutAll}/>
		</>

	)

}