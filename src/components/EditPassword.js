import { Button, Form, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';


export default function EditPassword({user}) {

	const [currentPass, setCurrentPass] = useState('');
	const [newPass, setNewPass] = useState('');
	const [confirmPass, setConfirmPass] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [message, setMessage] = useState('');

	const openEdit = () => {

		setShowEdit(true);

	}

	const closeEdit = () => {

		setCurrentPass('');
		setNewPass('');
		setConfirmPass('');
		setShowEdit(false);

	}

	useEffect(() => {

		const passwordRegex = /^(?=.*[!@#$%^&*(),.?":{}|<>])(?=.*[a-zA-Z0-9]).{8,}$/;

		if( currentPass !== '' && newPass !== '' && confirmPass !== '' ){

			if (newPass === confirmPass) {

				if (passwordRegex.test(newPass)){

					setIsActive(true);

				} else {

					setIsActive(false);
					setMessage('New password must be at least 8 characters long and contain at least one special character');

				}

			} else {

				setIsActive(false);
				setMessage('New passwords does not match');

			}


		} else {

			setIsActive(false);
			setMessage('Please complete your new password');

		}

	},[currentPass, newPass, confirmPass])

	const editPassword = (e, userId) => {

		e.preventDefault();

		if (!isActive) {
		    // Handle the case where the form is not valid
		    console.log('Form is not valid');
		    return;
		}

		fetch(`${ process.env.REACT_APP_API_URL }/users/${userId}/edit-pass`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				newPass: newPass,
				currentPass: currentPass
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Password Successfully Updated'
				})
				closeEdit();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: data.error
				})
			}

		})

	}
	
	return(

		<>

			<Button variant="primary" size="sm" onClick={() => openEdit()}> Change Password </Button>

			<Modal show={showEdit} onHide={closeEdit}>
				
				<Form onSubmit={e => editPassword(e, user)}>
					<Modal.Header closeButton>
					    <Modal.Title>Edit Password</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
						    <Form.Label>Current Password</Form.Label>
						    <Form.Control 
						    		type="password"
						    		value={currentPass}
						    		onChange={e => setCurrentPass(e.target.value)} 
						    		required/>
						</Form.Group>
						<Form.Group>
						    <Form.Label>New Password</Form.Label>
						    <Form.Control 
						    		type="password"
						    		value={newPass}
						    		onChange={e => setNewPass(e.target.value)} 
						    		required/>
						</Form.Group>
						<Form.Group>
						    <Form.Label>Confirm New Password</Form.Label>
						    <Form.Control 
						    		type="password"
						    		value={confirmPass}
						    		onChange={e => setConfirmPass(e.target.value)} 
						    		required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						{ isActive ?
					    	<Button variant="success" type="submit">Update</Button>
					    	:
						    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">{message}</Tooltip>}>
						    	<span className="d-inline-block">
						    		<Button variant="danger" type="submit" disabled style={{ pointerEvents: 'none' }}>Update</Button>
						    	</span>
						    </OverlayTrigger>
					    }
					</Modal.Footer>
				</Form>

			</Modal>


		</>

	)

}