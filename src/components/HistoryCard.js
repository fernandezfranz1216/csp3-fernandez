import { Card, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';


export default function HistoryCard({historyData}){

	const [history, setHistory] = useState([]);

	const options = {
	  year: 'numeric',
	  month: 'numeric',
	  day: 'numeric',
	  hour: 'numeric',
	  minute: 'numeric',
	  second: 'numeric',
	  timeZoneName: 'short'
	};

	useEffect(() => {

		const sortedHistory = [...historyData].sort((a, b) => new Date(b.purchasedOn) - new Date(a.purchasedOn));

		const historyArr = sortedHistory.map(hist => {

			return(

				<Col md={4} key={hist._id}>
					<Card className="my-2">
						<Card.Body>
							<Card.Title>{hist.productName}</Card.Title>
							<Card.Text>₱{hist.productPrice} x {hist.quantity} = {hist.productPrice * hist.quantity}</Card.Text>
							<Card.Text>{hist.status}</Card.Text>
							<Card.Text>{new Date(hist.purchasedOn).toLocaleString('en-US', options)}</Card.Text>
						</Card.Body>
					</Card>
				</Col>

			)
		})

		setHistory(historyArr);

	},[historyData])

	return (
		<>
			{history}
		</>

	)

}