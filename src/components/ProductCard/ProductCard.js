import { Card, Col, Button } from 'react-bootstrap';
import { useState } from 'react';
import PropTypes from 'prop-types';
import ProductView from '../ProductView/ProductView';
import './ProductCard.scss';

export default function ProductCard({ productProp, fetchData }) {

	const { _id, name, description, price, images } = productProp;

	const [showModal, setShowModal] = useState(false);

	const openModal = () => {
		setShowModal(true);
	}

	return (

		<Col md={4} className="text-center" id='card-container'>
			<Card className="my-2" id="productCard">
				<Card.Img id="prod-img" variant="top" src={`${images[0]}`} onClick={openModal}/>
				<Card.Body className='px-0'>
					<div className='mb-2'>
						<Card.Subtitle>{name.toUpperCase()}</Card.Subtitle>
						<Card.Subtitle>PHP {price}.00</Card.Subtitle>
					</div>
					<ProductView product={productProp} fetchData={fetchData} showModal={showModal} setShowModal={setShowModal}/>
				</Card.Body>	
			</Card>
		</Col>

	)

}


ProductCard.propTypes = {
	
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		images: PropTypes.arrayOf(PropTypes.string).isRequired,
		price: PropTypes.number.isRequired
	})

}