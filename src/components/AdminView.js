import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import CreateProduct from './CreateProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import '../App.css';


export default function AdminView({ productsData, fetchData }) {

	const [products, setProducts] = useState([]);


	useEffect(() => {

		const productsArr = productsData.map(products => {

			return (
			    <tr key={products._id}>
			        <td data-label="ID">{products._id}</td>
			        <td data-label="Name">{products.name}</td>
			        <td data-label="Description">{products.description}</td>
			        <td data-label="Price">{products.price}</td>
			        <td data-label="Quantity">{products.quantityAvailable}</td>
			        <td data-label="Image">
			        	{products.images.map((imageUrl, index) => (
			        		<div key={index}>
			        			{imageUrl} <br/>
			        		</div>
			        	))}
			        </td>
			        <td data-label="Availability" className={products.isActive ? "text-success" : "text-danger"}>
			            {products.isActive ? "Available" : "Unavailable"}
			        </td>
			        <td data-label="Actions"><EditProduct products={products._id} fetchData={fetchData} /></td> 
			        <td data-label="Actions"><ArchiveProduct products={{ productId: products._id, isActive: products.isActive }} fetchData={fetchData} /></td>
			    </tr>
			)	

		})

		setProducts(productsArr)

	}, [productsData])

	return(
	    <>
	        <h1 className="text-center my-4 text-light"> Admin Dashboard</h1>
	        <Container className="d-flex align-items-center justify-content-center">
	        	<CreateProduct fetchData={fetchData} />
	        </Container>
	        <Table striped bordered hover responsive className="mt-4">
	            <thead>
	                <tr className="text-center">
	                    <th>ID</th>
	                    <th>Name</th>
	                    <th>Description</th>
	                    <th>Price</th>
	                    <th>Quantity</th>
	                    <th>Image</th>
	                    <th>Availability</th>
	                    <th colSpan="2">Actions</th>
	                </tr>
	            </thead>

	            <tbody>
	                {products}
	            </tbody>
	        </Table>    
	    </>

	)

}