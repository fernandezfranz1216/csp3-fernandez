import { useState } from 'react';
import { Form, Button, Image, ListGroup, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function CreateProducts({ e, fetchData }) {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantityAvailable, setQuantityAvailable] = useState('');
	const [image, setImages] = useState("");

	const [show, setShow] = useState(false);

	// const handleImageChange = (e) => {

	//     const files = e.target.files;
	//     setImages([...images, ...files]);

	// };

	// const removeImage = (index) => {
	//     const newImages = [...images];
	//     newImages.splice(index, 1);
	//     setImages(newImages);
	//   };

	const openAdd = () => {

		setShow(true)

	}

	const closeAdd = () => {

		setShow(false)

	}

	function createProducts(e){

		e.preventDefault();

		let token = localStorage.getItem('token');

		// Create a FormData object to handle file uploads
		    // const formData = new FormData();
		    // formData.append('name', name);
		    // formData.append('description', description);
		    // formData.append('price', price);
		    // formData.append('quantityAvailable', quantityAvailable);
		    // formData.append('image', image);


		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			}, 
			body: JSON.stringify({
				name,
				description,
				price,
				quantityAvailable,
				image
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){

				Swal.fire({
					title: "Product Added",
					icon: "success"

				});

				fetchData();

			} else {

				Swal.fire({
					title: "Unsuccessful Product Creation",
					icon: "error",
					text: data.message
				});

			}

		})


		setName('');
		setDescription('');
		setPrice('');
		setImages('');


	}

	return(

		<>

			<Button variant="primary" size="sm" onClick={openAdd}> Create New Product </Button>

			<Modal show={show} onHide={closeAdd}>
				<Form onSubmit={e => createProducts(e)}>
					<Modal.Header closeButton>
					    <Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body> 
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Quantity:</Form.Label>
							<Form.Control type="number" placeholder="Enter Quantity" required value={quantityAvailable} onChange={e => {setQuantityAvailable(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Image:</Form.Label>
							<Form.Control type="text" placeholder="Enter image URL" required value={image} onChange={e => {setImages(e.target.value)}}/>
							{/*<Form.Control type="file" accept="image/*" multiple onChange={handleImageChange}	/>
							<ListGroup className="mt-2">
							            {images.map((image, index) => (
							              <ListGroup.Item key={index} className="d-flex justify-content-between align-items-center">
							                  <div>
							                      <Image src={URL.createObjectURL(image)} alt={`Image ${index + 1}`} rounded width={50} height={50} className="mr-2" />
							                      {image.name}
							                  </div>
							                  <div>
							                      <Button variant="danger" size="sm" onClick={() => removeImage(index)}>
							                          Remove
							                      </Button>
							                  </div>
							              </ListGroup.Item>
							            ))}
							</ListGroup>*/}
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" type="submit" className="my-2">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>

	)

}