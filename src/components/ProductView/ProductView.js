import { Button, InputGroup, Form, Modal, Image, Carousel, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import	Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import './ProductView.scss';

export default function ProductView({product, fetchData, showModal, setShowModal}) {

	const { _id, name, description, price, images, quantityAvailable } = product;

	const { user } = useContext(UserContext);

	const [quantity, setQuantity] = useState(0);
	const [quantityAvail, setQuantityAvail] = useState(quantityAvailable);
	const [isActivePlus, setIsActivePlus] = useState(true);
	const [isActiveMinus, setIsActiveMinus] = useState(true);
	const [isActive, setIsActive] = useState(false);
	const [showDetails, setShowDetails] = useState(false);

	const [index, setIndex] = useState(0);

	const handleSelect = (selectedIndex, e) => {
	    setIndex(selectedIndex);
	};

	useEffect(() => {

		if ( quantityAvail !== 0 ) {

			setIsActivePlus(true);


		} else {
			setIsActivePlus(false);
		}


		if (quantity !== 0) {

			setIsActive(true);
			setIsActiveMinus(true);

		} else {

			setIsActive(false);
			setIsActiveMinus(false);
		}
	},[quantityAvail, quantity]);


	const closeDetails = () => {
		setShowDetails(false);
	}

	function checkout(e, productId, quantityAvailable){
		e.preventDefault();

		console.log("quantityAvailable" + quantityAvailable);

		fetch(`${ process.env.REACT_APP_API_URL }/products/checkout`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId,
				quantity,
				quantityAvailable
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message === "Purchase Successful"){

				Swal.fire({
					title: "Purchase Successful",
					icon: "success",
					text: "You have successfully purchased this product."
				})

				setQuantity(0);
				setShowDetails(false);
				fetchData();

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})

			}

		})

	}

	function addToCart(productId, quantityAvailable){

		fetch(`${ process.env.REACT_APP_API_URL }/carts/add`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId,
				quantity,
				quantityAvailable
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message === "Product added to the cart successfully"){

				Swal.fire({
					title: "Add-to-cart Successful",
					icon: "success",
					text: "You have successfully added this product to your cart."
				})

				setQuantity(0);
				setShowDetails(false);
				fetchData();

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})

			}

		})

	}


	function handleQuantityChange(newQuantity, newAvailable) {

		setQuantity(newQuantity);
		setQuantityAvail(newAvailable);

	}

	function calculateTotalPrice() {

	    return price * quantity;

	 }



	return (

		<>
		{showModal && (
			<Modal id="modal" show={showModal} onHide={()=> setShowModal(false)} centered>
				<Form id='modal-form' onSubmit={e => checkout(e, _id, quantityAvail)}>
					<Modal.Header closeButton>
						<Modal.Title>{name}</Modal.Title>
					</Modal.Header>
					<Modal.Body id="modal-body">
						<Form.Group>
						<Carousel activeIndex={index} onSelect={handleSelect} data-bs-theme="dark">
							{images.map((image, i) => (
							<Carousel.Item key={i}>
								<Image src={`${ process.env.REACT_APP_API_URL }/images/${image}`} style={{width: '100%', height: '300px', objectFit: 'cover'}} alt={`Image ${i}`} fluid />
							</Carousel.Item>
							))}
						</Carousel>
						</Form.Group>
						<Form.Group>
							<Col className="text-center my-2">
								<Form.Label><h5>{description}</h5></Form.Label>
							</Col>
						</Form.Group>
						<Form.Group>
							<Row>
								<Col>
									<Form.Label>Price: </Form.Label>
								</Col>
								<Col>
									<Form.Label>Php {price}.00</Form.Label>
								</Col>
							</Row>
						</Form.Group>
						<Form.Group>
							<Row>
								<Col>
									<Form.Label>Stock:</Form.Label>
								</Col>
								<Col>
									<Form.Label>{quantityAvail}</Form.Label>
								</Col>
							</Row>
						</Form.Group>
						<Form.Group>
							<Row>
								<Col>
									<Form.Label>Quantity:</Form.Label>
								</Col>
								<Col>
									<InputGroup className="mb-3">

										{(isActiveMinus)?
											<Button variant="outline-secondary" onClick={() => handleQuantityChange((quantity-1),(quantityAvail+1))}>-</Button>
											:
											<Button variant="outline-secondary" onClick={() => handleQuantityChange((quantity-1),(quantityAvail+1))} disabled>-</Button>
										}

										<Form.Control type="number" value={quantity} readOnly />
										{(isActivePlus)?

											<Button variant="outline-secondary" onClick={() => handleQuantityChange((quantity+1),(quantityAvail-1))}>+</Button>
											:
											<Button variant="outline-secondary" onClick={() => handleQuantityChange((quantity+1),(quantityAvail-1))} disabled>+</Button>
										}
									</InputGroup>
								</Col>
							</Row>
						</Form.Group>
						<Form.Group>
							<Row>
								<Col>
									<Form.Label>Total Price:</Form.Label>
								</Col>
								<Col>
									<Form.Label>Php {calculateTotalPrice()}.00</Form.Label>
								</Col>
							</Row>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						{(user.id !== null) ? (
							(isActive) ? (
								<>
								<Button variant="success" onClick={() => addToCart(_id, quantityAvail)}>Add to Cart</Button>
								<Button variant="success" type="submit">Checkout</Button>
								</>

								) : (

								<>
								<Button variant="success" onClick={() => addToCart(_id, quantityAvail)} disabled>Add to Cart</Button>
								<Button variant="success" type="submit" disabled>Checkout</Button>
								</>
								)
							) : (
							<p>Login to purchase product!</p>
							)
						}
					</Modal.Footer>		
				</Form>
			</Modal>
		)}

		</>

	)

}