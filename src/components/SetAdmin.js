import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';


export default function SetAdmin({ usersData, fetchData }) {

	const { userId, isAdmin } = usersData;

	const setAsAdmin = (userId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-admin`, {
			method : "PUT",
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User Successfully Updated'
				})
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				fetchData();
			}

		})

	}

	const setAsUser = (userId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-user`, {
			method : "PUT",
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User Successfully Updated'
				})
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: ' Please try again'
				})
				fetchData();
			}

		})

	}

	return (

		isAdmin ?
		<Button variant="danger" size="sm" onClick={() => setAsUser(userId)}>Set As User</Button>
		:
		<Button variant="success" size="sm" onClick={() => setAsAdmin(userId)}>Set As Admin</Button>

	)

}